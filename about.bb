Thank you for playing my game. I hope you enjoyed it. Here are some of the special people that made this game possible.

[b]My Thanks To[/b]

First, my Family for putting up with me during this weekend. It was a very fun game to make, and it was fun to collaborate with my youngest son a little bit.

Crystal Images - for their crystal images.
https://www.deviantart.com/oni1ink/art/Tutorial-How-to-draw-Crystals-586241499

Godot Stopwatch
https://gitlab.com/swolford/godot-stopwatch

JPMusic82 - Drumloop House 128 bpm
https://freesound.org/people/JPMusic82/sounds/274466/

Godot
https://www.gosotengine.com 

Check out my Godot Gitlab Repository
https://gitlab.com/godot-stuff

[b]Version History[]/b]

v0.0.3
- made hit boxes slightly bigger for minerals and smaller for obstacles
- fixed some of the text in the help screen

v0.0.2
- added ui text to show hull and temp guages
- keep ui visible when game is over 
- high score entry fixed (in case someone makes it)

v0.0.1
- first release