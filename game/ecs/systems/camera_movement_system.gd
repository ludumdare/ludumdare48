class_name CameraMovementSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _movement = entity.get_component("movement") as Movement
	
#	entity.position += Vector2.DOWN * (_movement.speed * (Game.base_speed + Game.next_speed)) * delta
	entity.position += Vector2.DOWN * _movement.speed * Game.next_speed * delta
	
