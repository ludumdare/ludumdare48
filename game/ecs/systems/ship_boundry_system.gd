#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: ShipControlSystem
#
#		Check Player Input to Control Ship
#
#

class_name ShipBoundrySystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _movement = entity.get_component("movement") as Movement
	var _boundry = entity.get_component("boundry") as Boundry
	
	if (entity.position.x <= _boundry.Left):
		entity.position.x = _boundry.Left
		
	if (entity.position.x >= _boundry.Right):
		entity.position.x = _boundry.Right
				
