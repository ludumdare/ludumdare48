class_name TrailDropSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _drop = Game.drop_trail()
	_drop.global_position = entity.global_position
