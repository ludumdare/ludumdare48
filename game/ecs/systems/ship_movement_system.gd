#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: ShipMovementSystem
#
#		Will Move Entities with a Ship Component
#		in a direction based on Direction and Speed
#

class_name ShipMovementSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _movement = entity.get_component("movement") as Movement
	
	var _speed = (_movement.speed * _movement.speed_factor) * delta
	
	if (Game.next_speed < 0):
		_speed = _movement.speed_factor * delta 
	
	entity.position += _movement.direction * _speed
	entity.position.y = lerp(entity.position.y, 200, delta)
