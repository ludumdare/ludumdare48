class_name LayerManagerSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _layermanager = entity.get_component("layermanager") as LayerManager
	var _layer = _layermanager.layer as LayerResource
	
	var _distance = Game.miles_per_cycle * Game.next_speed
	
	_layermanager.mile += _distance
	_layermanager.total_miles += _distance
	
	if (_layermanager.total_miles > _layer.thickness):
#	if (_layermanager.total_miles > 1):
		
		Game.layer += 1
		
		if (Game.layer >= Game.layers.size()):
			return 
			
		_layermanager.layer = Game.layers[Game.layer]
		_layermanager.total_miles = 0
		Game.layer_data = Game.layers[Game.layer]
		
		
	Logger.fine(_layer.name)
	Logger.fine("layer_mile: %s; total_miles: %s" % [Game.layer_mile, Game.total_miles])
	
	
	if (_layermanager.mile >= 0.8):
		
		# add new segment
		
		_layermanager.mile = 0
		var _seg = _layer.segments[rand_range(0, _layer.segments.size())].instance()
		add_child(_seg)
		var _seg_layer = Layer.new()
		_seg_layer.id = _layer.id
		_seg_layer.hull = _layer.hull
		_seg_layer.temp = _layer.temp
		_seg.set_meta ("layer", _seg_layer)
		_seg.global_position = Vector2(0, Game.ship.global_position.y) + Vector2(0,1000)

		# populate segment with goodies
		
		var _total = rand_range(0, 4) + 1
		var _pickups = _layer.pickups
		var _weight : float  = 0
		
		for _pickup in _pickups:
			_weight += _pickup.weight
			_pickup.acc_weight = _weight
				
		for i in range(_total):
			var _roll = rand_range(0.0, _weight)
			for _pickup in _pickups:
				if (_pickup.acc_weight > _roll):
					
					var _x = int(rand_range(0, 6))
					var _y = int(rand_range(5, 7))
					
					var _p = _get_pickup(_pickup.pickup)
					_seg.add_child(_p)
					_p.position = Vector2(_x, _y) * 100
					
					print(_p.position)
					
					break
					
func _get_pickup(id):
	
	if (id == "cobalt"): return Game.PickupCobalt.instance()
	if (id == "gas"): return Game.PickupGas.instance()
	if (id == "tungsten"): return Game.PickupTungsten.instance()
	if (id == "quartz"): return Game.PickupQuartz.instance()
	if (id == "saphire"): return Game.PickupSaphire.instance()
	if (id == "diamond"): return Game.PickupDiamond.instance()
	if (id == "rock"): return Game.PickupRock.instance()
	
	if (id == "lava"): 
		var _node = Node2D.new()
		var _lava1 = Game.PickupLava.instance()
		var _lava2 = Game.PickupLava.instance()
		var _lava3 = Game.PickupLava.instance()
		_node.add_child(_lava1)
		_node.add_child(_lava2)
		_lava2.position += Vector2.LEFT * 50
		_node.add_child(_lava3)
		_lava3.position += Vector2.RIGHT * 50
		return _node
	
	return null
