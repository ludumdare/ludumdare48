class_name ShipCollisionSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _collides = entity.get_component("collides") as Collides
	
	var _area_objects = entity.get_overlapping_areas()
	var _body_objects = entity.get_overlapping_bodies()
	
	if (_area_objects.size() <= 0 and _body_objects.size() <= 0):
		return
	
	for _obj in _area_objects:
		
		if (_obj.name.find("Pickup") >= 0):
			var _pickup = _obj.get_component("pickup")
			Logger.debug(_pickup.name)
			Game.emit_signal("on_pickup", _pickup)
			
			_remove_coll(_obj)
			
			if (_pickup.type == "lava"):
				break
				
			if (_pickup.type == "rock"):
				break
				 
			ECS.remove_entity(_obj)
			
		if (_obj.name.find("Layer") >= 0):
			var _layer = _obj.get_meta("layer")
			Logger.debug(_layer.name)
			Game.emit_signal("on_layer", _layer)
			
			
	for _obj in _body_objects:
		
		# layer - get parent entity
		if (_obj.name.find("Layer") >= 0):
			var _layer = _obj.get_parent().get_meta("layer")
			
			if (!_layer):
				return
				
			Logger.debug(_layer.id)
			Game.emit_signal("on_layer", _layer)
			
			_remove_coll(_obj)
			

func _remove_coll(obj):
	
	var _coll = obj.get_node("CollisionPolygon2D")
	if (_coll):
		_coll.set_disabled(true)
		
	_coll = obj.get_node("CollisionShape2D")
	if (_coll):
		_coll.set_disabled(true)
	
