#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: ShipControlSystem
#
#		Check Player Input to Control Ship
#
#

class_name ShipControlSystem
extends System

func on_process_entity(entity: Entity, delta: float):
	
	var _movement = entity.get_component("movement") as Movement
	_movement.direction = Vector2.ZERO
	
	if (Input.is_action_pressed("game_left")):
		_movement.direction = Vector2.LEFT

	if (Input.is_action_pressed("game_right")):
		_movement.direction = Vector2.RIGHT
