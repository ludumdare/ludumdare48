class_name TrailFollowSystem
extends System

func on_process_entity(entity : Entity, delta : float):
	
	var _trail = entity.get_component("trail")
	entity.position = _trail.target.global_position
	
