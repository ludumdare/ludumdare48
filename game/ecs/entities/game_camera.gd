extends Entity

onready var camera = $Camera2D

func _ready():
	Game.camera = self


func small_shake():
	$Camera2D/ScreenShake.start(0.25, 40, 5, 0)

	
func big_shake():
	$Camera2D/ScreenShake.start(1.0, 40, 10, 0)


func reverse():
	camera.zoom = Vector2(1, -1)
	camera.offset = Vector2(0, 960)
	$Camera2D/ScreenShake.normal_offset = Vector2(0, 960)
	
