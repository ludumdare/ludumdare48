#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: Movement
#
#		Component for Entities that will Move
#

class_name Movement
extends Component

# current speed of the entity
export var speed : float = 10.0

# speed factor
export var speed_factor : float = 10.0

# entity drift when moving
export var drift : float = 1.5

# max speed
export var max_speed : float = 20.0

# min speed
export var min_speed : float = 5.0

# direction
var direction : Vector2 = Vector2.ZERO

