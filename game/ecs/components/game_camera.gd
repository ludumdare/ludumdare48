#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: GameCamera
#
#		Component for an Entity that is a Game Camera.
#

class_name GameCamera
extends Component
