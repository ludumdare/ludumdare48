#
# 	Copyright 2021 SpockerDotNet LLC, All rights reserved.
#
#	Class: Ship
#
#		Component for Entities that Represent the Ship.
#
#	Remarks:
#
#		This is a label component intended for the Players
#		Ship entity.
#

class_name Ship
extends Component
