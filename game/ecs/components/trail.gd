class_name Trail
extends Component

# the name of the Node to Lookup
export var follows : String = "Ship"

# cycle drop rate - how many cycles before drop
export var drop_rate : int = 10

# the Node to Follow
var target : Node 

# current cycle
var drop_cycle : int = 0

