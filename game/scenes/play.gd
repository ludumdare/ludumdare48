extends Node2D

onready var debug = $CanvasLayer/UI/Play/Debug
onready var stopwatch = $Stopwatch

onready var ui_play = $CanvasLayer/UI/Play
onready var ui_pause = $CanvasLayer/UI/Pause
onready var ui_win = $CanvasLayer/UI/Win
onready var ui_lose = $CanvasLayer/UI/Lose
onready var ui_message = $CanvasLayer/UI/Message

var state
var last_state

func _process(delta):
	
	if (Input.is_action_just_pressed("game_pause")):
		
		if (Core.state == Game.PLAY_STATE_NONE):
			Core.change_state(last_state)
		else:
			last_state = Core.state
			Core.change_state(Game.PLAY_STATE_NONE)
			
	if (Input.is_action_just_pressed("cheat_win")):
		Core.change_state(Game.PLAY_STATE_WIN)
		
	if (Input.is_action_just_pressed("cheat_lose")):
		Core.change_state(Game.PLAY_STATE_LOSE)
		
	
	match state:
		
		Game.PLAY_STATE_INIT:
			Game.layer = 0
			Game.score = 0
			Game.total_miles = 0
			Game.track_miles = false
			Game.temp = 0
			Game.hull = 100
			_hide_ui()
			ui_play.show()
			Core.change_state(Game.PLAY_STATE_WARMUP)
		
		Game.PLAY_STATE_START:
			stopwatch.start()
			Core.change_state(Game.PLAY_STATE_RUN)
			
		Game.PLAY_STATE_STOP:
			pass
			
		Game.PLAY_STATE_NONE:
			stopwatch.pause()
			pass
			
		Game.PLAY_STATE_LOSE:
			_hide_ui()
			ui_play.show()
			ui_lose.show()
			
		Game.PLAY_STATE_WIN:
			_hide_ui()
			ui_win.show()
			ui_play.show()
			ui_win.focus()
			
		Game.PLAY_STATE_WARMUP:
			ECS.update("warmup")
			Game.next_speed = lerp(Game.next_speed, Game.base_speed + Game.speed, delta)
			
		Game.PLAY_STATE_HOME:
			ECS.clean()
			ECS.update("init")
			Core.change_state(Ludum.GAME_STATE_HOME)
			
		Game.PLAY_STATE_RUN:
			
			if (stopwatch.paused):
				stopwatch.resume()
				
			Game.time = stopwatch.get_formatted_elapsed_time(null, "%02d", ":%02d", null)
			Game.time_raw = stopwatch.get_elapsed_time()
			
			ECS.update("run")
			
			Game.next_speed = lerp(Game.next_speed, Game.base_speed + Game.speed, .25 * delta)
			
			
			if (Game.track_miles):
				var _distance = Game.miles_per_cycle * Game.next_speed
#				var _distance = (Game.miles_per_cycle * (Game.base_speed + Game.next_speed)) * Game.reverse_speed
				Game.total_miles += _distance
				
			if (Game.hull <= 0):
				Core.change_state(Game.PLAY_STATE_LOSE)
				
			if (Game.temp >=1000):
				Core.change_state(Game.PLAY_STATE_LOSE)

	if (!Game.debug):
		return 
						
	var _d = ""
	
	_d += "\nScore: %s" % Game.score
	_d += "\nHi: %s" % Game.hiscore
	_d += "\nHull: %s" % Game.hull
	_d += "\nTemp: %s" % Game.temp
	_d += "\nSpeed: %s (%s)" % [Game.speed, Game.base_speed+ Game.speed]
	_d += "\nNext Speed: %s" % [Game.next_speed]
	_d += "\nMiles: %s" % Game.total_miles
	_d += "\nLayer Mile: %s" % Game.layer_mile
	_d += "\nPlay State: %s" % state
	_d += "\nGame State: %s" % Core.state
	_d += "\nStopwatch: %s" % stopwatch.get_formatted_elapsed_time()
	_d += "\nShip Y Pos: %s" % Game.ship.position.y
	_d += "\nLayer: %s" % Game.layer_data.name
	
	debug.text = _d
	

func _init():
	ECS.clean()
	ECS.update("init")
	
	
func _ready():
	Core.connect("state_changed", self, "_on_state_changed")
	Game.connect("on_layer", self, "_on_layer")
	Game.connect("on_pickup", self, "_on_pickup")
	Core.change_state(Game.PLAY_STATE_INIT)
	

func _on_state_changed(new_state):
	state = new_state
	
	
func _on_layer(layer : Layer):
	$Audio/Layer.play()


func _on_pickup(pickup : Pickup):
	
	if(pickup.id == "gas"):
		$Audio/Gas.play()
		return
		
	if(pickup.id == "rock"):
		$Audio/Rock.play()
		return
		
	if(pickup.id == "lava"):
		$Audio/Lava.play()
		return
		
	if(pickup.type == "hull"):
		$Audio/Hull.play()
		return
		
	if(pickup.type == "temp"):
		$Audio/Temp.play()
		return
		
	$Audio/Pickup.play()
		
		
func _hide_ui():
	ui_play.hide()
	ui_pause.hide()
	ui_message.hide()
	ui_lose.hide()
	ui_win.hide()
