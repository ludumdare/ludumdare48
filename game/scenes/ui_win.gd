extends Control


func focus():
	$TextEdit.grab_focus()
	
func _ready():
	pass


func _on_Retry_pressed():
	Core.change_state(Ludum.GAME_STATE_PLAY)


func _on_Home_pressed():
	Core.change_state(Game.PLAY_STATE_HOME)


func _on_Submit_pressed():
	
	var _name = $TextEdit.text
	
	if (_name.length() == 0):
		return

	var _score = Game.score
	var _time = "%0d" % int(Game.time_raw)
	var _miles = "%0.1f" % Game.total_miles
	DreamLo.add_score_time_text(_name, _score, _time, _miles)
	Core.change_state(Game.PLAY_STATE_HOME)
