extends Control

onready var score = $Score/_Score
onready var hiscore = $HiScore/_HiScore
onready var miles = $Miles/_Miles
onready var multiplier = $Multiplier/_Multiplier
onready var time = $_Time

func _process(delta):
	
	score.text = "%s" % Game.score 
	hiscore.text = "%s" % Game.hiscore	
	miles.text = "%0.1f" % Game.total_miles
	multiplier.text = "x%s" % Game.speed
	time.text = Game.time 
	
#	Game.score += 1
#	Game.hiscore += 1
#	Game.total_miles += 1
#	Game.speed += 1
