extends Node2D

export var settings_config : String = "res://game.cfg"
export var dreamlo_config : String = "res://dreamlo.cfg"
export var windowmanager_config : Resource 
export (Array, Resource) var layers
export var max_speed = 15
export var min_speed = 1


var TrailDrop = preload("res://game/ecs/entities/trail_drop.tscn")
var PickupTest = preload("res://game/ecs/entities/pickup.tscn")
var PickupCobalt = preload("res://game/ecs/entities/pickup_cobalt.tscn")
var PickupGas = preload("res://game/ecs/entities/pickup_gas.tscn")
var PickupTungsten = preload("res://game/ecs/entities/pickup_tungsten.tscn")
var PickupLava = preload("res://game/ecs/entities/pickup_lava.tscn")
var PickupQuartz = preload("res://game/ecs/entities/pickup_quartz.tscn")
var PickupSaphire = preload("res://game/ecs/entities/pickup_saphire.tscn")
var PickupDiamond = preload("res://game/ecs/entities/pickup_diamond.tscn")
var PickupRock = preload("res://game/ecs/entities/pickup_rock.tscn")

var ship
var layer
var camera
var layer_data

var layer_mile : float
var total_miles : float
var track_miles : bool = false

var base_speed : int = 5
var speed : int = 1
var next_speed : float = 0
var miles_per_cycle : float = .00175

var temp : int = 0
var hull : int = 0
var score : int = 0
var hiscore : int = 0
var time : String = ""
var time_raw : float
var debug : bool = false

var reverse : bool = false
var reverse_speed = 1

const PLAY_STATE_NONE = "PLAY_STATE_NONE"
const PLAY_STATE_START = "PLAY_STATE_START"
const PLAY_STATE_STOP = "PLAY_STATE_STOP"
const PLAY_STATE_WARMUP = "PLAY_STATE_WARMUP"
const PLAY_STATE_RUN = "PLAY_STATE_RUN"
const PLAY_STATE_INIT = "PLAY_STATE_INIT"
const PLAY_STATE_PAUSE = "PLAY_STATE_PAUSE"
const PLAY_STATE_WIN = "PLAY_STATE_WIN"
const PLAY_STATE_LOSE = "PLAY_STATE_LOSE"
const PLAY_STATE_HOME = "PLAY_STATE_HOME"

signal on_pickup(pickup)
signal on_layer(layer)

func _ready():
	
	Ludum.start(settings_config, LudumSettingsResource.new())
	DreamLo.load_settings(dreamlo_config)
	WindowManager.load(windowmanager_config)
	
	connect("on_pickup", self, "_on_pickup")
	connect("on_layer", self, "_on_layer")
	
	layer_data = layers[0]


func drop_trail():
	var _drop = TrailDrop.instance() 
	get_tree().get_root().add_child(_drop)
	return _drop


func _on_pickup(pickup : Pickup):
	
	Logger.debug(pickup.type)
	
	score += pickup.points * speed # point * multiplier
	
	if (pickup.type == "speed"): 
		speed += pickup.value
		next_speed = base_speed + speed + 6
		ship.position.y += 50
	
	if (pickup.type == "hull"): 
		hull += pickup.value
	
	if (pickup.type == "temp"): 
		temp += pickup.value 
		
	if (pickup.id == "lava"):
		temp += 10
		hull -= 5
		speed = 1
#		next_speed = next_speed / 2
		next_speed = -1
	
	if (pickup.id == "rock"):
		hull -= 5
		speed = 1
#		next_speed = next_speed / 4
		next_speed = -2
		camera.big_shake()
		
	if (speed < min_speed):
		speed = min_speed

	if (speed > max_speed):
		speed = max_speed
		
	if (temp < 0):
		temp = 0
			
#	if (next_speed < 0):
#		next_speed = 0	


func _on_layer(layer : Layer):
	
	Logger.info(layer.name)
	
	# when we hit the first layer start
	# the actual gameplay
	
	if (!track_miles):
		track_miles = true 
		Core.change_state(Game.PLAY_STATE_START)
		return
		
	if (layer.id == "end"):
#		Game.total_miles = 0
		Core.change_state(Game.PLAY_STATE_WIN)
		return
		
#	if (layer.id == "center"):
#		camera.reverse()
#		camera.big_shake()
#		reverse = true
#		reverse_speed = -1
		
	hull -= layer.hull 
	temp += layer.temp
		
#	next_speed = next_speed * 0.95

	if (temp < 0):
		temp = 0
			
	camera.small_shake()
