class_name LayerResource
extends Resource

export var id : String = "id"
export var name : String = "Name"
export var thickness : int = 0
export var next_layer : String = ""
export var temp : int = 0
export var hull : int = 0
export (Array, PackedScene) var segments
export (Array, Resource) var pickups
