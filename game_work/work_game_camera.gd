extends Node2D

func _process(delta):
	
	if (Input.is_action_just_released("ui_up")):
		Game.speed += 1
		
	if (Input.is_action_just_pressed("ui_down")):
		Game.speed -= 1
		
	Logger.debug("%s" % Game.speed)
